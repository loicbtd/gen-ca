# GEN-CA, a simple SSL certificates generator

## How it works

1. Create your settings directory based on the **settings.example**

```shell
cp -r ./settings.example ./settings
```

2. Edit the settings files as you wish


3. Give to the script **genca** the execution rights

```shell
chmod +x ./gen-ca
```

4. Run the script

```shell
./gen-ca
```